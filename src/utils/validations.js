import { validateEmail } from "./";
import { validate } from "./rut";

export const EmailValid = [
  {
    field: false,
    validateFunction: (val, current) => {
      return validateEmail(current);
    },
    error: "Ingrese un email válido",
  },
];

export const RutValid = [
  {
    field: false,
    validateFunction: (val, current) => {
      return validate(current);
    },
    error: "Rut , invalido",
  },
];
