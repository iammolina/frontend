export const handleChangeForm = (
  forms,
  name,
  value,
  directComponentError = false
) => {
  let resultForms = Object.assign({}, forms);
  const isSelect = forms[name].inputType === "Select";
  value = isSelect && value ? parseInt(value, 10) : value;
  const { validations } = forms[name];
  if (validations && validations.length > 0) {
    for (const validation of validations) {
      const {
        field,
        validateFunction,
        validationType,
        error,
        notAllowWrite,
      } = validation;
      const fieldToCompare = field ? forms[field].value : false;
      const isValid = validateFunction(fieldToCompare, value);
      if (validationType === "equal" && isValid) {
        resultForms[field].error = "";
      }
      resultForms = Object.assign({}, resultForms, {
        [name]: {
          ...forms[name],
          error: !isValid
            ? error
            : directComponentError
            ? directComponentError
            : "",
          value: !isValid && notAllowWrite ? forms[name].value : value,
        },
      });
      if (!isValid) {
        break;
      }
    }
  } else {
    resultForms = Object.assign({}, resultForms, {
      [name]: {
        ...forms[name],
        error: directComponentError ? directComponentError : "",
        value,
      },
    });
  }
  return resultForms;
};
export const handleAddButton = (
  form,
  setForm,
  currentAddField,
  keyOriginal,
  newKey,
  newItemForm,
  newIndex
) => {
  const newAddField = {
    ...form[currentAddField],
    props: {
      ...form[currentAddField].props,
      field: {
        ...form[currentAddField].props.field,
        index: newIndex,
      },
    },
  };
  delete form[currentAddField];

  return setForm(
    Object.assign({}, form, {
      [keyOriginal]: {
        ...form[keyOriginal],
      },
      [newKey]: {
        ...newItemForm,
      },
      [currentAddField]: newAddField,
    })
  );
};
export const validateEmail = (value) => {
  const invalidEmails = ["gmail.cl", "hotmail.cl"];
  const invalid = invalidEmails.some((str) => {
    return value.includes(str);
  });
  if (invalid) {
    return false;
  }
  return /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,5})+$/.test(value);
};
export const validateEqual = (firstValue, secondValue) => {
  return firstValue === secondValue;
};

export const validateOnlyNumbersAndLetters = (value) => {
  if (value === "") return true;
  return /^[a-zA-Z0-9]+$/.test(value);
};

export const validateOnlyNumbers = (value) => {
  if (value === "") return true;
  return /^\d+$/.test(value);
};
