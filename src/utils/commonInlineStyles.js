export const styles = {
  content: {
    paddingTop: 50,
    paddingBottom: 50,
    marginTop: 5,
    marginBottom: 10,
  },
  marginContent: {
    marginTop: 20,
    marginBottom: 100,
  },
  wrapper: {
    margin: "0 auto",
  },
  blueBackground: {
    background: "var(--main-blue)",
  },
  secondBlueBackground: {
    background: "var(--second-blue)",
  },
  greenBackground: {
    background: "var(--main-green)",
  },
  redBackground: {
    background: "var(--main-red)",
  },
  imageContainer: {
    width: "100px",
    "& > img": {
      display: "block",
      width: "100%",
      height: "100%",
    },
  },
};
export default styles;
