import React from "react";
import { Route, withRouter } from "react-router-dom";
import appRoutes from "./appRoutes";
import Layout from "../screens/layout";
import { Switch } from "react-router-dom";

function Routes() {
  const { privado } = appRoutes;
  return (
    <>
      <Switch>
        <Layout routes={privado}>
          <Switch>
            {privado.map((routeProps) => (
              <Route {...routeProps} />
            ))}
          </Switch>
        </Layout>
      </Switch>
    </>
  );
}

export default withRouter(Routes);
