import Crud from "../screens/crud";
import Login from "../screens/login";
import Home from "../screens/home";
import AddFactura from "../screens/facturas/add";
import EditFactura from "../screens/facturas/edit";
import AddBoleta from "../screens/boletas/add";
import EditBoleta from "../screens/boletas/edit";
import AddArriendos from "../screens/arriendos/add";
import EditArriendos from "../screens/arriendos/edit";

const privateRoutes = [
  {
    url: `/crud/libros`,
    path: `/crud/:entity`,
    key: "home-view",
    component: Crud,
    text: "Libros",
    exact: false,
    mostrar: true,
  },
  {
    url: `/crud/categorias`,
    path: `/crud/:entity`,
    key: "categorias-view",
    component: Crud,
    text: "Categorias",
    exact: false,
    mostrar: true,
  },
  {
    url: `/crud/isbn`,
    path: `/crud/:entity`,
    component: Crud,
    key: "isbn-view",
    text: "Isbn",
    exact: false,
    mostrar: true,
  },
  {
    url: `/crud/librosEstados`,
    path: `/crud/:entity`,
    component: Crud,
    key: "estados-view",
    text: "Estados",
    exact: false,
    mostrar: true,
  },
  {
    url: `/crud/editorial`,
    path: `/crud/:entity`,
    component: Crud,
    key: "editorial-view",
    text: "Editorial",
    exact: false,
    mostrar: true,
  },
  {
    url: `/crud/autores`,
    path: `/crud/:entity`,
    component: Crud,
    key: "autores-view",
    text: "Autores",
    exact: false,
    mostrar: true,
  },
  {
    url: `/crud/metodoDePago`,
    path: `/crud/:entity`,
    component: Crud,
    key: "metododepago-view",
    text: "Metodos de Pago",
    exact: false,
    mostrar: true,
  },
  {
    url: `/crud/distribuidores`,
    path: `/crud/:entity`,
    component: Crud,
    key: "distribuidores-view",
    text: "Distribuidores",
    exact: false,
    mostrar: true,
  },
  {
    url: `/crud/idiomas`,
    path: `/crud/:entity`,
    component: Crud,
    key: "idiomas-view",
    text: "Idiomas",
    exact: false,
    mostrar: true,
  },
  {
    url: `/crud/clientes`,
    path: `/crud/:entity`,
    component: Crud,
    key: "clientes-view",
    text: "clientes",
    exact: false,
    mostrar: true,
  },
  {
    url: `/crud/trabajadores`,
    path: `/crud/:entity`,
    component: Crud,
    key: "trabajadores-view",
    text: "trabajadores",
    exact: false,
    mostrar: true,
  },
  {
    url: `/crud/facturas`,
    path: `/crud/:entity`,
    component: Crud,
    key: "facturas-view",
    text: "Facturas",
    exact: false,
    mostrar: true,
  },
  {
    url: `/crud/compras`,
    path: `/crud/:entity`,
    component: Crud,
    key: "compras-view",
    text: "Compras",
    exact: false,
    mostrar: true,
  },
  {
    url: `/add/facturas`,
    path: `/add/facturas`,
    key: "addfactura-view",
    component: AddFactura,
    exact: true,
    mostrar: false,
  },
  {
    url: `/edit/facturas`,
    path: `/edit/facturas/:id`,
    key: "editfasctura-view",
    component: EditFactura,
    exact: true,
    mostrar: false,
  },
  {
    url: `/crud/boletas`,
    path: `/crud/:entity`,
    component: Crud,
    key: "boletas-view",
    text: "Boletas",
    exact: false,
    mostrar: true,
  },
  {
    url: `/crud/ventas`,
    path: `/crud/:entity`,
    component: Crud,
    key: "ventas-view",
    text: "Ventas",
    exact: false,
    mostrar: true,
  },
  {
    url: `/add/boletas`,
    path: `/add/boletas`,
    key: "addboletas-view",
    component: AddBoleta,
    exact: true,
    mostrar: false,
  },
  {
    url: `/edit/boletas`,
    path: `/edit/boletas/:id`,
    key: "editboletas-view",
    component: EditBoleta,
    exact: true,
    mostrar: false,
  },
  {
    url: `/crud/arriendos`,
    path: `/crud/:entity`,
    component: Crud,
    key: "arriendos-view",
    text: "Arriendos",
    exact: false,
    mostrar: true,
  },

  {
    url: `/add/arriendos`,
    path: `/add/arriendos`,
    key: "addArriendos-view",
    component: AddArriendos,
    exact: true,
    mostrar: false,
  },
  {
    url: `/edit/arriendos`,
    path: `/edit/arriendos/:id`,
    key: "editArriendos-view",
    component: EditArriendos,
    exact: true,
    mostrar: false,
  },
];

export const sharedRoutes = [
  {
    path: ["/", "/home"],
    key: "home-view",
    component: Login,
    exact: true,
    privado: false,
  },
  {
    path: ["/other"],
    key: "home-view",
    component: Home,
    exact: true,
  },
];

export const appRoutes = {
  publico: [...sharedRoutes],
  privado: [...privateRoutes],
};

export default appRoutes;
