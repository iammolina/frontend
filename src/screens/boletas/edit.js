import React, { useState, useEffect } from "react";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core";
import { withRouter } from "react-router-dom";
import { Table, Typography as antTypography, Descriptions } from "antd";
import NumberFormat from "react-number-format";
import API from "../../config/api";
import Content from "../content";
import { Loading } from "../../components";

const { Text } = antTypography;
const homeStyles = makeStyles((theme) => ({
  button: {
    textDecoration: `none`,
    marginBottom: theme.spacing(2),
    [theme.breakpoints.down(`sm`)]: {},
  },
  descripcion: {
    padding: theme.spacing(2),
  },
}));

function EditBoleta({
  history,
  match: {
    params: { id },
  },
}) {
  const classes = homeStyles();
  const [loading, setLoading] = useState(false);
  const [items, setItems] = useState([]);
  const [detalle, setDetalle] = useState({
    clienteName: ``,
    trabajadorName: ``,
    metodoName: ``,
    fechaVenta: ``,
  });

  const columns = [
    {
      title: "Index",
      render: (text, record, index) => {
        return index + 1;
      },
    },
    {
      title: "Libro",
      render: (item) => {
        return item && item.name;
      },
    },
    {
      title: "Cantidad",
      render: (item) => {
        return item && item.cantidad;
      },
    },
    {
      title: "Valor",
      render: (item) => {
        return item && item.valor;
      },
    },
    {
      title: "Sub Total",
      render: (item) => {
        return item && item.valor * item.cantidad;
      },
    },
  ];

  const getData = async (id) => {
    setLoading(true);
    const data = await API.getCustomEntity(`boleta/form/${id}`);
    setDetalle(data.detalle);
    setItems(data.items);
    setLoading(false);
  };

  useEffect(() => {
    id && getData(id);
    // eslint-disable-next-line
  }, [id]);

  if (loading) {
    return <Loading />;
  }

  return (
    <Content>
      <Grid container item xs={12} className={classes.descripcion}>
        <Descriptions title={`Boleta folio : ${detalle.folio}`} bordered>
          <Descriptions.Item label="Cliente">
            {detalle.clienteName}
          </Descriptions.Item>
          <Descriptions.Item label="Trabajador">
            {detalle.trabajadorName}
          </Descriptions.Item>
          <Descriptions.Item label="Metodo de Pago">
            {detalle.metodoName}
          </Descriptions.Item>
          <Descriptions.Item label="Fecha Boleta">
            {detalle.fechaBoleta}
          </Descriptions.Item>
        </Descriptions>
        ,
      </Grid>
      <Grid item xs={12}>
        <Table
          loading={false}
          dataSource={items}
          columns={columns}
          summary={(pageData) => {
            let totalBorrow = 0;
            let totalRepayment = 0;
            let totalCantidad = 0;
            let totalIva = 0;

            pageData.forEach(({ valor, cantidad }) => {
              totalRepayment += valor * cantidad;
              totalBorrow += parseInt(valor);
              totalIva += parseInt(valor * 0.19);
              totalCantidad += parseInt(cantidad);
            });

            return (
              <>
                <Table.Summary.Row>
                  <Table.Summary.Cell>Total</Table.Summary.Cell>
                  <Table.Summary.Cell></Table.Summary.Cell>
                  <Table.Summary.Cell>{totalCantidad}</Table.Summary.Cell>
                  <Table.Summary.Cell>
                    <Text type="danger">
                      <NumberFormat
                        decimalSeparator={"."}
                        value={totalBorrow}
                        displayType={"text"}
                        thousandSeparator={true}
                        prefix={"$"}
                      />
                    </Text>
                  </Table.Summary.Cell>

                  <Table.Summary.Cell>
                    <Text type="danger">
                      <NumberFormat
                        decimalSeparator={"."}
                        value={totalIva}
                        displayType={"text"}
                        thousandSeparator={true}
                        prefix={"$"}
                      />
                    </Text>
                  </Table.Summary.Cell>

                  <Table.Summary.Cell>
                    <Text>
                      <NumberFormat
                        decimalSeparator={"."}
                        value={totalRepayment + totalIva * totalCantidad}
                        displayType={"text"}
                        thousandSeparator={true}
                        prefix={"$"}
                      />
                    </Text>
                  </Table.Summary.Cell>
                </Table.Summary.Row>
              </>
            );
          }}
        />
      </Grid>
    </Content>
  );
}

export default withRouter(EditBoleta);
