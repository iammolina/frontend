export const formAdd = {
  libro: {
    value: ``,
    error: false,
    label: "Libro",
    placeholder: "Escriba aquí",
    inputType: "TextInput",
    inputRequired: true,
  },
  cantidad: {
    value: ``,
    error: false,
    label: "Cantidad de libros",
    placeholder: "Escriba aquí",
    inputType: "TextInput",
    type: "Number",
    inputRequired: true,
  },
  valor: {
    value: ``,
    error: false,
    label: "Valor libros",
    placeholder: "Escriba aquí",
    inputType: "TextInput",
    type: "Number",
    inputRequired: true,
  },
};
