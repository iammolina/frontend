import { validateEmail } from "../../utils";

export const formLogin = {
  email: {
    value: ``,
    error: false,
    label: "Correo electrónico",
    placeholder: "Escriba aquí",
    inputType: "TextInput",
    inputRequired: true,
    validations: [
      {
        field: false,
        validateFunction: (val, current) => {
          return validateEmail(current);
        },
        error: "Ingrese un email válido",
      },
    ],
  },
  password: {
    value: ``,
    error: false,
    label: "Password",
    placeholder: "Escriba aquí",
    inputType: "TextInput",
    type: "password",
    inputRequired: true,
  },
};
