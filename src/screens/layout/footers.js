import React from "react";
import { Grid, Typography, makeStyles } from "@material-ui/core";
import FavoriteIcon from "@material-ui/icons/Favorite";
import { Link } from "react-router-dom";
import Rss from "../../components/rss";

const footerStyles = makeStyles((theme) => ({
  footer: {
    backgroundColor: theme.palette.terniary.main,
    padding: "0 20px",
    minHeight: 200,
  },
  logo: {
    width: 90,
  },
  secondary: {
    minHeight: 50,
    backgroundColor: theme.palette.terniary.light,
  },
  squaresContainer: {
    padding: "10px 0 0 0",
    width: "250px",
  },
  square: {
    marginTop: theme.spacing(1),
    padding: "5px 0",
    width: "11%",
    [theme.breakpoints.down("sm")]: {
      width: "15%",
    },
  },
  containerItems: {
    [theme.breakpoints.down("sm")]: {
      width: "100%",
    },
  },
}));

export default function Footer() {
  const classes = footerStyles();
  return (
    <>
      <Grid
        container
        justify="center"
        alignItems="center"
        direction="row"
        className={`${classes.footer} content`}
      >
        <Grid item md={2}></Grid>

        <Grid item md={5}>
          <Typography variant="h5" color="inherit">
            <Link to={"/tienda"} className="link-white">
              Tienda
            </Link>
          </Typography>
          <Typography variant="h5" color="inherit">
            <Link to={"/questions"} className="link-white">
              Preguntas Frecuentes
            </Link>
          </Typography>
          <Typography variant="h5" color="inherit">
            <Link to={"/contact"} className="link-white">
              {" "}
              Contactanos{" "}
            </Link>
          </Typography>
        </Grid>
        <Grid item md={4}>
          <Grid
            container
            justify="center"
            alignItems="center"
            direction="column"
            spacing={2}
          >
            <Grid item>
              <Typography variant="h5" color="inherit">
                Informacion General
              </Typography>
            </Grid>
            <Grid item>
              <Typography variant="h6" color="inherit">
                contacto@altisimo.cl
              </Typography>
            </Grid>
            <Grid item>
              <Rss size="default" type="link-white" />
            </Grid>
          </Grid>
        </Grid>
        <Grid item md={1}>
          <img src={"/img/a-01.jpg"} alt="logo2" className={classes.logo} />
        </Grid>
      </Grid>
      <Grid
        container
        justify="center"
        alignItems="center"
        className={classes.secondary}
      >
        <Typography variant="h6" className={classes.phagraph}>
          Hecho con <FavoriteIcon color="error" />
          por iamdev
        </Typography>
      </Grid>
    </>
  );
}
