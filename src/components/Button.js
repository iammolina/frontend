import React from 'react'
import Button from '@material-ui/core/Button'
import CircularProgress from '@material-ui/core/CircularProgress'

function ButtonWrapper({ loading = false, children, ...props }) {
  return <Button {...props}>{loading ? <CircularProgress /> : children}</Button>
}

export default ButtonWrapper
