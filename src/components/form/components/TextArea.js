import React from "react";
import InputWrapper from "./InputWrapper";
import TextareaAutosize from "@material-ui/core/TextareaAutosize";

const TextAreaWrapper = ({
    id,
    label,
    error,
    isRequired = false,
    onChange,
    value,
    name,
    placeholder,
    disabled = false,
    align = `left`,
    md = 12,
    xs = 12,
    rows = 4,
    rowsMax = 10,
    ...rest
}) => {
    return (
        <InputWrapper
            label={label}
            isRequired={isRequired}
            align={align}
            error={error}
            md={md}
            xs={xs}
            disabled={disabled}
        >
            <TextareaAutosize
                style={{ width: "100%", border: "1px solid grey" }}
                rowsMax={rowsMax}
                rows={rows}
                aria-label="maximum height"
                placeholder={placeholder}
                value={value}
                id={name}
                name={name}
                disabled={disabled}
                onChange={({ target: { value, name } }) =>
                    onChange({ name, value, id })
                }
                error={error}
                {...rest}
            />
        </InputWrapper>
    );
};

export default TextAreaWrapper;
