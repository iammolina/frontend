import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Checkbox from "@material-ui/core/Checkbox";
import InputWrapper from "./InputWrapper";

const useStyles = makeStyles(({ palette }) => ({
    checked: {
        "& svg": {
            fill: palette.primary.main,
        },
    },
    root: {
        fontSize: `18px`,
    },
}));

const CheckboxWrapper = ({
    label,
    error,
    isRequired = false,
    onChange,
    value,
    name,
    disabled = false,
    align = `left`,
    fullWidth = false,
    md = 12,
    xs = 12,
    callbacks = null,
    ...rest
}) => {
    const classes = useStyles();
    const onChangeHandle = ({ target: { name } }) => {
        if (typeof onChange === `function`) {
            onChange({ name, value: !value });
        } else {
            const { callback, fieldsToUpdate } = onChange;
            callbacks[callback]({ fieldsToUpdate, value: !value, name });
        }
    };
    return (
        <InputWrapper
            label={label}
            error={error}
            isRequired={isRequired}
            align={align}
            fullWidth={fullWidth}
            positionLabel="right"
            md={md}
            xs={xs}
            disabled={disabled}
            {...rest}
        >
            <Checkbox
                disabled={disabled}
                checked={value}
                onChange={onChangeHandle}
                name={name}
                className={classes.root}
            />
        </InputWrapper>
    );
};

export default CheckboxWrapper;
