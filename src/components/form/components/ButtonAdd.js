import React from "react";
import { Grid, makeStyles } from "@material-ui/core";
import LinkButton from "../custom/LinkButton";

function ButtonAdd({ props, addNewAction }) {
    const classes = styles();

    return (
        <Grid
            container
            item
            className={classes.buttonAddContainer}
            justify="center"
        >
            <Grid item>
                {props.field.index < props.max - 1 && (
                    <LinkButton
                        onClick={() =>
                            addNewAction(props.field, props.currentField)
                        }
                        buttonText={props.buttonText}
                    />
                )}
            </Grid>
        </Grid>
    );
}

const styles = makeStyles((theme) => ({
    buttonAddContainer: {
        padding: theme.spacing(1),
    },
}));

export default ButtonAdd;
