import React, { useState } from "react";

import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import { makeStyles } from "@material-ui/core/styles";
import InputWrapper from "./InputWrapper";
import { TextInput } from ".";

const styles = makeStyles((theme) => ({
    input: {
        display: "none",
    },
    button: {
        margin: 0,
        height: 53,
        borderRadius: 0,
    },
    error: {
        textAlign: `center`,
        color: theme.palette.error.main,
        lineHeight: `1.6em`,
        fontSize: `1.4em`,
        marginTop: theme.spacing(2),
    },
    container: {
        [theme.breakpoints.up(`sm`)]: {
            paddingRight: theme.spacing(1),
        },
    },
}));

function FileUpload({
    id,
    label,
    error, //Puede ser boolean o string
    isRequired = false,
    onChange,
    onBlur = null,
    value,
    name,
    min = 0,
    autocompleteData,
    max,
    inputType = "file",
    acceptTypes = "image/*",
    fileTypes = [".jgp", ".png"],
    size = "medium",
    inputProps = {},
    InputProps = {},
    disabled = false,
    placeholder = ``,
    align = `left`,
    multiple = false,
    fullWidth = false,
    maxMBFileSize = 2,
    xs = 12,
    md = 12,
    ...rest
}) {
    const classes = styles();

    const [state, setState] = useState({
        upload: { value: ``, error: false },
    });

    const onChangeFields = ({ name, value }) => {
        setState({
            ...state,
            [name]: { error: ``, value },
        });
    };

    const uploadFile = () => {
        const fileUpload = document.querySelector(`#${name}-upload`);
        fileUpload.click();
    };
    const validTypes = (type) => {
        return fileTypes.some((ft) => ft === type);
    };

    const handleUploadClick = (event) => {
        const file = event.target.files[0];
        if (!file) {
            return;
        }
        const validSize = file.size / 1024 / 1024 <= maxMBFileSize;
        const validType = validTypes(file.type);
        if (!validSize || !validType) {
            setState({
                ...state,
                upload: {
                    ...state.upload,
                    error: `${
                        !validSize
                            ? `El archivo no debe pesar más de ${maxMBFileSize}MB`
                            : `Tipo de archivo inválido`
                    }`,
                },
            });
            return onChange({
                name,
                value: "",
                directComponentError: "error",
            });
        }

        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onloadend = function (e) {
            setState({
                ...state,
                upload: { ...state.upload, value: file.name, error: false },
            });
            onChange({
                name,
                value: multiple
                    ? [reader.result]
                    : {
                          base64: reader.result,
                          name: file.name,
                          type: file.type,
                      },
                directComponentError: false,
            });
        };
    };
    return (
        <InputWrapper
            label={label}
            isRequired={isRequired}
            align={align}
            type={inputType}
            error={error}
            fullWidth={fullWidth}
            xs={xs}
            md={md}
            disabled={disabled}
            {...rest}
        >
            <input
                accept={acceptTypes}
                className={classes.input}
                id={`${name}-upload`}
                multiple={multiple}
                type="file"
                onChange={handleUploadClick}
            />
            <Grid container item className={classes.container}>
                <Grid item xs={9} md={11}>
                    <TextInput
                        error={state.upload.error}
                        placeholder={placeholder}
                        type="text"
                        onChange={({ name, value }) =>
                            onChangeFields({ name, value })
                        }
                        fullWidth={true}
                        name={`${name}-upload`}
                        inputProps={{ readOnly: true }}
                        onClick={uploadFile}
                        value={state.upload.value}
                    />
                </Grid>
                <Grid item xs={3} md={1}>
                    <Button
                        color="primary"
                        variant="contained"
                        className={classes.button}
                        onClick={uploadFile}
                    >
                        Elegir
                    </Button>{" "}
                </Grid>
            </Grid>
            {state.upload.error && state.upload.error.length > 0 && (
                <Grid container item justify="center">
                    <Grid item>
                        <Typography className={classes.error}>
                            {state.upload.error}
                        </Typography>
                    </Grid>
                </Grid>
            )}
        </InputWrapper>
    );
}

export default FileUpload;
