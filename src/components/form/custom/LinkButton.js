import React from "react";
import { Grid, Button, makeStyles } from "@material-ui/core";

function BackButton({ onClick, justify = "flex-start", buttonText }) {
    const classes = styles();

    return (
        <Grid container item justify={justify}>
            <Grid item>
                <Button
                    className={`${classes.linkButton}`}
                    variant="outlined"
                    onClick={onClick}
                >
                    {buttonText}
                </Button>
            </Grid>
        </Grid>
    );
}

const styles = makeStyles(() => ({
    linkButton: {
        background: `none`,
        color: `#042E48!important`,
        border: `none !important`,
        textDecoration: `underline`,
        textTransform: "none",
        "&:hover": {
            transition: `none !important`,
            textDecoration: `underline`,
            background: `none !important`,
            color: `#042E48!important`,
            border: `none !important`,
        },
    },
}));

export default BackButton;
