import React from "react";
import { Grid, Typography, makeStyles } from "@material-ui/core";

function CustomHeader({ props }) {
    const classes = styles();

    return (
        <Grid container item className={classes.header}>
            <Grid item xs={12} sm={12} lg={12} xl={12}>
                <Typography
                    variant="h2"
                    className={`${classes.text} ${classes.title}`}
                >
                    {props.headerText}
                </Typography>
            </Grid>
        </Grid>
    );
}

const styles = makeStyles((theme) => ({
    header: {
        backgroundColor: theme.palette.secondary.main,
        padding: theme.spacing(1),
    },
    text: {
        color: "#ffffff",
    },
    title: {
        fontWeight: "bold",
    },
    backgroundPrimary: {
        backgroundColor: "beige",
        padding: theme.spacing(2, 2) + `!important`,
    },
    container: {
        padding: theme.spacing(2, 0),
    },
}));

export default CustomHeader;
