import React, { useState, useEffect } from "react";
import Table from "antd/es/table";

function TableComponent({
  loading = false,
  pagination,
  customId = ``,
  classes = ``,
  scroll = { x: 500 },
  style = { width: `100%` },
  paginationSettings = {
    defaultCurrent: 1,
    hideOnSinglePage: false,
    pageSizeOptions: [`5`, `10`, `20`, `50`, `100`, `500`],
    showSizeChanger: true,
  },
  dataSource,
  columns,
  getData = () => null,
  emptyText = `Sin datos`,
  bordered = false,
  expanded = null,
  expandedRowRender = null,
  onChange = false,
  ...other
}) {
  const [pageSize, setPageSize] = useState(10);

  const onShowSizeChange = (page, pageSize) => {
    loading = true;
    setPageSize(pageSize);
    getData(page, pageSize);
  };
  const onChangePage = (page, pageSize) => {
    loading = true;
    getData(page, pageSize);
  };
  useEffect(() => {
    if (pagination && pagination.perPage) {
      setPageSize(parseInt(pagination.perPage, 10));
    }
  }, [pagination, setPageSize]);

  return (
    <Table
      {...other}
      id={customId}
      bordered={bordered}
      scroll={scroll}
      style={style}
      pagination={
        pagination
          ? {
              ...paginationSettings,
              total: pagination.total,
              currentPage: pagination.currentPage,
              pageSize,
              onShowSizeChange: onShowSizeChange,
              onChange: onChangePage,
            }
          : false
      }
      columns={columns}
      dataSource={dataSource}
      loading={loading}
      locale={{ emptyText }}
      rowKey={(record, i) => `${i}-${record.id}`}
      className={classes}
      expandedRowRender={expanded ? expandedRowRender : null}
      onChange={
        onChange
          ? (e, i, x, currentDataSource) => onChange(currentDataSource)
          : null
      }
    />
  );
}

export default TableComponent;
