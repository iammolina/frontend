import React from "react";
import { ThemeProvider } from "@material-ui/core/styles";
import theme from "./config/theme";
import Private from "./routes/private";
import Login from "./screens/login";
import { BrowserRouter as Router } from "react-router-dom";
import { useAuth } from "./context";

function App() {
  const { user } = useAuth();
  return (
    <Router>
      <ThemeProvider theme={theme}>
        {!user ? <Login /> : <Private />}
      </ThemeProvider>
    </Router>
  );
}

export default App;
