import { createMuiTheme } from "@material-ui/core/styles";

const fontWeights = [300, 400, 500, 700];

export const colors = {
  primary: { main: `#0067c6`, light: `#2396ff`, dark: `#42a4ff` },
  secondary: { main: `#0072db`, light: `#42a4ff` },
  terniary: { main: `#212121`, light: `#E3E3E3` },
  grayRgba: [
    `rgba(224, 224, 224, 1)`, // 0
    `rgba(0, 111, 179, 0.08)`, // 1
    `rgba(80,227,194,0)`, // 2
    `rgba(0, 0, 0, 0.08)`, //3
    `rgba(0, 0, 0, 0.12)`, //4
    `rgba(0, 0, 0, 0.14)`, //5
    `rgba(0, 0, 0, 0.26)`, //6
    `rgba(0, 0, 0, 0.38)`, //7
    `rgba(0,0,0,0.50)`, //8
    `rgba(0, 0, 0, 0.54)`, //9
  ],
  gray: [`#F6F7FB`, `#D8D8D8`, `#979797`],
  error: { main: `#E02020` },
  success: { main: `#328A0E` },
};
const fontSizes = [
  18, //0 H5
  20, //1 H4
  22, //2 H3
  30, //3 H2
  40, //4 H1
];
const familyRoboto = [`Helvetica`, `Roboto`, `sans-serif`].join(`,`);

const theme = createMuiTheme({
  palette: {
    primary: colors.primary,
    secondary: colors.secondary,
    terniary: colors.terniary,
    black: colors.primary.black,
    error: colors.error,
    gray: { main: `#AAAEB7`, dark: `#73787e` },
    success: colors.success,
    text: {
      primary: colors.primary.main,
      secondary: colors.secondary.main,
      disabled: colors.grayRgba[7],
      hint: colors.grayRgba[7],
    },
    background: {
      default: colors.gray[0],
    },
    action: {
      active: colors.grayRgba[9],
      hover: colors.grayRgba[3],
      hoverOpacity: 0.08,
      selected: colors.grayRgba[5],
      disabled: colors.grayRgba[6],
      disabledBackground: colors.grayRgba[4],
    },
    type: `light`,
  },
  fontSizes,
  fontWeights,
});
theme.typography = {
  ...theme.typography,
  htmlFontSize: 10,
  fontSize: fontSizes[1],
  fontFamily: familyRoboto,
  h1: {
    fontSize: fontSizes[4],
    fontWeight: fontWeights[2],
    lineHeight: `4.5rem`,
    letterSpacing: -0.21,
  },
  h2: {
    fontSize: fontSizes[3],
    fontWeight: fontWeights[1],
    lineHeight: `2.8rem`,
    [theme.breakpoints.down(`sm`)]: {
      fontSize: fontSizes[3],
      lineHeight: `2.5rem`,
    },
  },
  h3: {
    fontSize: fontSizes[2],
    fontWeight: fontWeights[1],
    lineHeight: `2.5rem`,
  },
  h4: {
    fontSize: fontSizes[2],
    fontWeight: fontWeights[1],
    lineHeight: `2.2rem`,
    opacity: 0.8,
  },
  h5: {
    fontSize: fontSizes[1],
    fontWeight: fontWeights[1],
    lineHeight: `2.0rem`,
    opacity: 0.8,
  },
  h6: {
    fontSize: `1.0rem`,
    lineHeight: `1.6rem`,
    fontWeight: 400,
    letterSpacing: 0,
  },
  body1: {
    fontSize: fontSizes[1],
    fontWeight: fontWeights[1],
    lineHeight: `${fontSizes[3]}px`,
  },
  body2: {
    fontSize: fontSizes[0],
    fontWeight: fontWeights[1],
    lineHeight: `${fontSizes[2]}px`,
  },
  subtitle1: {
    fontSize: fontSizes[2],
    fontWeight: fontWeights[1],
    lineHeight: `3rem`,
    color: colors.secondary.main,
  },
  subtitle2: {
    fontSize: fontSizes[1],
    fontWeight: fontWeights[1],
    lineHeight: `2.5rem`,
    opacity: 0.8,
  },
  caption: {
    fontSize: fontSizes[1],
    fontWeight: fontWeights[1],
    lineHeight: `1.8rem`,
    cursor: `pointer`,
    "&:hover": {
      color: colors.terniary.main,
      textDecoration: `underline`,
    },
  },
};
theme.overrides = {
  MuiButton: {
    text: {
      color: theme.palette.secondary.main,
      border: `${theme.palette.primary.main} 1px solid`,
      borderRadius: 0,
      minWidth: 50,
      minHeight: 50,
      textTransform: `none`,
      textDecoration: `none`,
      fontSize: fontSizes[2],
      fontWeight: 400,
      "&:hover": {
        textDecoration: `none`,
        backgroundColor: theme.palette.terniary.main,
        color: theme.palette.secondary.main,
      },
    },
    contained: {
      backgroundColor: theme.palette.primary.main,
      color: `#000 !important`,
      boxShadow: `0 2px 4px 0 rgba(0,0,0,0.50)`,
      "&:hover": {
        textDecoration: `underline`,
        backgroundColor: `#00B199 !important`,
        color: `#000 !important`,
      },
    },
    outlined: {
      backgroundColor: theme.palette.secondary.main,
      color: `#fff !important`,
      "&:hover": {
        backgroundColor: `#65ACD1 !important`,
        color: `#fff !important`,
      },
    },
  },
  MuiLink: {
    textDecoration: `underline`,
    color: colors.primary.dark,
  },
  MuiExpansionPanelSummary: {
    root: {
      backgroundColor: theme.palette.terniary.light,
      color: theme.palette.terniary.main,
    },
    expanded: {
      backgroundColor: "white",
      color: theme.palette.terniary.main,
    },
    content: {
      alignItems: `center`,
      margin: `20px 0`,
    },
  },
  MuiInputBase: {
    root: {
      color: theme.palette.terniary.main,
    },
  },
  MuiSvgIcon: {
    fontSizeLarge: {
      fontSize: 50,
    },
    colorAction: {
      color: "white",
    },
  },
  MuiTableCell: {
    head: {
      color: theme.palette.terniary.main,
    },
    body: {
      color: theme.palette.terniary.main,
    },
  },
  MuiTypography: {
    colorInherit: {
      color: theme.palette.terniary.light,
    },
    colorPrimary: {
      color: theme.palette.terniary.main,
    },
    colorSecondary: {
      color: theme.palette.secondary.main,
    },
  },
};
export default theme;
