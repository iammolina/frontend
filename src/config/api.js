import axios from "axios";
axios.defaults.headers.common["Access-Control-Allow-Origin"] = "*";
axios.defaults.baseURL = process.env.REACT_APP_API_URL;
axios.defaults.headers.common["Authorization"] =
  localStorage.getItem("token") || null;

const API = {};
const axiosInstance = axios.create({
  validateStatus() {
    return true;
  },
  withCredentials: true,
});
const axiosInstanceDownloadPdf = axios.create({
  validateStatus() {
    return true;
  },
  withCredentials: true,
});

axiosInstance.interceptors.response.use(
  (response) => {
    const {
      status = 200,
      data: { data, message = "Hubo un error", code = 200 },
    } = response;
    if (status !== 200) {
      if (code === 401) {
        window.location = `/`;
      } else {
        return Promise.reject({ message });
      }
    } else {
      return data;
    }
  },
  (error) => Promise.reject(error)
);
API.me = () => axiosInstance.get(`auth/me`);
API.login = (body) => axiosInstance.post(`auth/login`, { ...body });
//Entity
API.getEntity = (entity) => axiosInstance.get(`/entidad/${entity}`);
API.getEntityList = (entity) => axiosInstance.get(`/entidad/${entity}/list`);
API.updateEntity = (id, entity, body) =>
  axiosInstance.put(`entidad/${entity}/${id}`, { ...body });
API.createEntity = (entity, body) =>
  axiosInstance.post(`entidad/${entity}`, { ...body });
API.getForm = (entity) => axiosInstance.get(`/entidad/${entity}/form`);
API.deleteEntity = (id, entity) =>
  axiosInstance.delete(`entidad/${entity}/${id}`);

API.getEntityCustom = (url) => axiosInstance.get(`${url}`);

API.postEntityCustom = (url, body) => axiosInstance.post(`${url}`, { ...body });

//Custom Entity
API.getCustomEntity = (entity) => axiosInstance.get(`/${entity}`);
API.getCustomEntityList = (entity) => axiosInstance.get(`/${entity}/list`);
API.updateCustomEntity = (id, entity, body) =>
  axiosInstance.put(`${entity}/${id}`, { ...body });
API.createCustomEntity = (entity, body) =>
  axiosInstance.post(`${entity}`, { ...body });

API.downloadPdf = (url, nombreDocumento = `postulacion`, type = `pdf`) => {
  return axiosInstanceDownloadPdf
    .get(url, { responseType: "arraybuffer" })
    .then((response) => {
      let mimeType = null;
      if (type === `xlsx`) {
        mimeType = `vnd.ms-excel`;
      } else if (type === `pdf`) {
        mimeType = `pdf`;
      }
      const blob = new Blob([response.data], {
        type: `application/${mimeType}`,
      });
      const link = document.createElement(`a`);
      link.href = window.URL.createObjectURL(blob);
      link.download = `${nombreDocumento}.${type}`;
      link.click();
    })
    .catch((error) => {
      console.log(error);
    });
};

export default API;
